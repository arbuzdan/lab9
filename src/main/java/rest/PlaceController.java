package rest;

import application.PlaceService;
import data.model.PlaceEntity;
import rest.dto.PlaceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@RestController
@ExposesResourceFor(PlaceDto.class)
@RequestMapping(value = "/api/places", produces = {MediaTypes.HAL_JSON_UTF8_VALUE, MediaType.APPLICATION_JSON_UTF8_VALUE})
public class PlaceController {
    static final ResourceAssemblerSupport<PlaceEntity, PlaceDto> ENTITY_TO_DTO
            = new ResourceAssemblerSupport<PlaceEntity, PlaceDto>(PlaceController.class, PlaceDto.class) {
        @Override
        public PlaceDto toResource(PlaceEntity e) {
            PlaceDto p = new PlaceDto(e.getName(), e.getContinent());
            p.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PlaceController.class).readByName(p.getName())).withSelfRel());
            return p;
        }
    };
    @Autowired
    private PlaceService service;
    @Autowired
    private EntityLinks entityLinks;
    private Function<PlaceDto, PlaceEntity> dtoToEntity = d -> new PlaceEntity(d.getName(), d.getContinent());

    @PostMapping
    public HttpEntity<PlaceDto> create(@RequestBody PlaceDto dto) {
        PlaceEntity entitySaved = service.createOrUpdate(dtoToEntity.apply(dto));
        PlaceDto result = ENTITY_TO_DTO.toResource(entitySaved);
        return ResponseEntity.created(entityLinks.linkForSingleResource(PlaceDto.class, result.getName()).toUri()).body(result);
    }

    @GetMapping
    public List<PlaceDto> readAll() {
        return ENTITY_TO_DTO.toResources(service.readAll());
    }

    @GetMapping("/{name}")
    public HttpEntity<PlaceDto> readByName(@PathVariable String name) {
        Optional<PlaceDto> resultOpt = service.readById(name).map(ENTITY_TO_DTO::toResource);
        Link linkToList = entityLinks.linkToCollectionResource(PlaceDto.class).withRel("list");
        if (resultOpt.isPresent()) {
            PlaceDto result = resultOpt.get();
            result.add(linkToList);
            return ResponseEntity
                    .ok()
                    .header(HttpHeaders.LINK, linkToList.toString())
                    .header(HttpHeaders.LINK, result.getLink("self").toString())
                    .body(result);
        } else
            return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
    }

    @PutMapping("/{name}")
    public HttpEntity<PlaceDto> updateOrCreate(@PathVariable String name, @RequestBody PlaceDto dto) {
        dto.setName(name);
        PlaceEntity entitySaved = service.createOrUpdate(dtoToEntity.apply(dto));
        PlaceDto result = ENTITY_TO_DTO.toResource(entitySaved);
        Link linkToList = entityLinks.linkToCollectionResource(PlaceDto.class).withRel("list");
        result.add(linkToList);
        return ResponseEntity
                .noContent()
                .header(HttpHeaders.LINK, result.getLink("self").toString())
                .header(HttpHeaders.LINK, linkToList.toString())
                .build();
    }

    @DeleteMapping("/{name}")
    public HttpEntity delete(@PathVariable String name) {
        Link linkToList = entityLinks.linkToCollectionResource(PlaceDto.class).withRel("list");
        try {
            service.deleteById(name);
            return ResponseEntity.noContent().header(HttpHeaders.LINK, linkToList.toString()).build();
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.notFound().header(HttpHeaders.LINK, linkToList.toString()).build();
        }
    }
}
