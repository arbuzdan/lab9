package data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQuery(name = "PlaceEntity.findAllVisitorsByName", query = "SELECT p.visitors FROM PlaceEntity p WHERE p.name = :name")
public class PlaceEntity {
    @Id
    private String name;

    private String continent;

    @ManyToMany(mappedBy = "visitedPlaces")
    private Collection<CharacterEntity> visitors;

    public PlaceEntity() {
    }

    public PlaceEntity(String name) {
        this.name = name;
    }

    public PlaceEntity(String name, String continent) {
        this.name = name;
        this.continent = continent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public Collection<CharacterEntity> getVisitors() {
        return visitors;
    }

    public void setVisitors(Collection<CharacterEntity> visitors) {
        this.visitors = visitors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlaceEntity placeEntity = (PlaceEntity) o;
        return name.equals(placeEntity.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
