package data.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import java.util.Collection;
import java.util.Objects;

@Entity
@NamedQuery(name = "CharacterEntity.findAllVisitedPlacesByFirstName", query = "SELECT c.visitedPlaces FROM CharacterEntity c WHERE c.firstName = :firstName")
public class CharacterEntity {
    @Id
    private String firstName;

    private String lastName;

    @ManyToMany
    private Collection<PlaceEntity> visitedPlaces;

    public CharacterEntity() {
    }

    public CharacterEntity(String firstName) {
        this.firstName = firstName;
    }

    public CharacterEntity(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Collection<PlaceEntity> getVisitedPlaces() {
        return visitedPlaces;
    }

    public void setVisitedPlaces(Collection<PlaceEntity> visitedPlaces) {
        this.visitedPlaces = visitedPlaces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CharacterEntity characterEntity = (CharacterEntity) o;
        return firstName.equals(characterEntity.firstName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName);
    }
}