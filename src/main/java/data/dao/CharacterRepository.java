package data.dao;

import data.model.CharacterEntity;
import data.model.PlaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CharacterRepository extends JpaRepository<CharacterEntity, String> {
    Collection<PlaceEntity> findAllVisitedPlacesByFirstName(String firstName);
}
