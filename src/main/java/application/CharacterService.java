package application;

import data.dao.CharacterRepository;
import data.dao.PlaceRepository;
import data.model.CharacterEntity;
import data.model.PlaceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class CharacterService {
    @Autowired
    private CharacterRepository repository;

    @Autowired
    private PlaceRepository placeRepository;

    @Transactional
    public CharacterEntity createOrUpdate(CharacterEntity e) {
        return repository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<CharacterEntity> readAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<CharacterEntity> readById(String firstName) {
        return repository.findById(firstName);
    }

    @Transactional(readOnly = true)
    public Collection<PlaceEntity> readVisitorsByName(String firstName) {
        return repository.findAllVisitedPlacesByFirstName(firstName);
    }

    @Transactional
    public void addVisitedPlace(String firstName, String placeName) {
        Optional<CharacterEntity> optionalCharacter = repository.findById(firstName);
        Optional<PlaceEntity> optionalPlace = placeRepository.findById(placeName);
        if (optionalCharacter.isPresent() && optionalPlace.isPresent()) {
            CharacterEntity character = optionalCharacter.get();
            PlaceEntity place = optionalPlace.get();
            character.getVisitedPlaces().add(place);
            repository.save(character);
        } else
            throw new IllegalArgumentException("character or place do not exist");
    }

    @Transactional
    public void removeVisitedPlace(String firstName, String placeName) {
        Optional<CharacterEntity> optionalCharacter = repository.findById(firstName);
        Optional<PlaceEntity> optionalPlace = placeRepository.findById(placeName);
        if (optionalCharacter.isPresent() && optionalPlace.isPresent()) {
            CharacterEntity character = optionalCharacter.get();
            PlaceEntity place = optionalPlace.get();
            character.getVisitedPlaces().remove(place);
            repository.save(character);
        } else
            throw new IllegalArgumentException("character or place do not exist");
    }

    @Transactional
    public void deleteById(String firstName) {
        repository.deleteById(firstName);
    }
}