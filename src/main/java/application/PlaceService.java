package application;

import data.dao.CharacterRepository;
import data.dao.PlaceRepository;
import data.model.CharacterEntity;
import data.model.PlaceEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;

@Service
public class PlaceService {
    @Autowired
    private PlaceRepository repository;

    @Autowired
    private CharacterRepository characterRepository;

    @Transactional
    public PlaceEntity createOrUpdate(PlaceEntity e) {
        return repository.save(e);
    }

    @Transactional(readOnly = true)
    public Collection<PlaceEntity> readAll() {
        return repository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<PlaceEntity> readById(String name) {
        return repository.findById(name);
    }

    @Transactional(readOnly = true)
    public Collection<CharacterEntity> readVisitorsByName(String name) {
        return repository.findAllVisitorsByName(name);
    }

    @Transactional
    public void deleteById(String name) {
        repository.deleteById(name);
    }
}